A DSA is needed for the following source packages in old/stable. The specific
CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

Some packages are not tracked here:
- Linux kernel (tracking in kernel-sec repo)
- Embargoed issues continue to be tracked in separate file.

To pick an issue, simply add your uid behind it.

If needed, specify the release by adding a slash after the name of the source package.

--
389-ds-base (fw)
--
asterisk/stable
  berni working on updates
--
chromium-browser
--
dokuwiki/oldstable
--
enigmail
--
ffmpeg/stable
  Wait for next 3.2.x release
--
gitlab
  Pirate Praveen will prepare updates
--
glusterfs
--
graphicsmagick
--
knot-resolver
--
libav/oldstable
  We can ship the next libav 11.x point release when available
--
libidn
--
linux
  Wait until more issues have piled up
--
mariadb-10.0/oldstable
--
mariadb-10.1/stable
--
mercurial
--
mosquitto (seb)
  2018-02-27: Roger Light provided a debdiff targetting stretch, needs review
--
openjdk-7/oldstable (jmm)
--
openjpeg2 (luciano)
--
packagekit
  Matthias Klumpp (mak) proposed debdiff for CVE-2018-1106
--
passenger/stable
--
php5/oldstable
--
php7.0/stable
--
php-horde-image
--
phpmyadmin/oldstable (abhijith)
  https://mentors.debian.net/debian/pool/main/p/phpmyadmin/phpmyadmin_4.2.12-2+deb8u3.dsc
--
procps (carnil)
--
qemu/oldstable
--
ruby2.1/oldstable
  Santiago will prepare an update
  work-in-progress: https://salsa.debian.org/ruby-team/ruby/tree/jessie-security-wip
--
ruby2.3/stable
  Santiago will prepare an update
  work-in-progress: https://salsa.debian.org/ruby-team/ruby/tree/stretch-security-wip
--
sssd/stable
--
thunderbird
--
tomcat7/oldstable
--
tomcat8 (seb)
  2018-04-11: Emmanuel Bourg submitted a debdiff
--
wavpack (jmm)
--
zendframework/oldstable
--
